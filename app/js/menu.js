'use strict';

const menuMobile = document.querySelector('.burger__menu');
const primaryNav = document.querySelector('.primary-nav');
const logoTitle = document.querySelector('.logo-title');
const tooltipMenu = document.querySelectorAll('.tooltip__menu');
const headMenuIndex = document.querySelectorAll('.menu__show');
const subcontent = document.querySelectorAll('.subcontent');
const subcontentWrapper = document.querySelectorAll('.subcontent__wrapper');
const tooltipInner = document.querySelectorAll('.tooltip__inner');
const hoverUl = document.querySelector('.nav__container');
let baseHeight = tooltipMenu.clientHeight - 9;
const menuShow = 980;

//мобильное меню
menuMobile.addEventListener('click', () => {
    menuMobile.classList.toggle('burger__menu--active');
    primaryNav.classList.toggle('active');
    logoTitle.classList.toggle('logo-title--color');
    tooltipMenu.forEach((items) => {
        items.classList.remove('tooltip__menu_show');
    });
});

if (window.innerWidth > menuShow) {
    for (let i = 0; i < headMenuIndex.length; i++) {
        headMenuIndex[i].addEventListener('mouseover', (event) => {
            tooltipMenu[i].classList.add('tooltip__menu_show');
            headMenuIndex[i].style.height = baseHeight + 'px';
            headMenuIndex[i].classList.add('menu__show--active');
            subcontent.forEach((elem) => {
                elem.classList.remove('subcontent--show');
            });
            subcontentWrapper.forEach((elem) => {
                elem.classList.remove('subcontent__wrapper--active');
            });
        });

        headMenuIndex[i].addEventListener('mouseout', (event) => {
            tooltipMenu[i].classList.remove('tooltip__menu_show');
            headMenuIndex[i].classList.remove('menu__show--active');
        });

        tooltipMenu[i].addEventListener('mouseover', (event) => {
            tooltipMenu[i].classList.add('tooltip__menu_show');
        });
        tooltipMenu[i].addEventListener('mouseout', (event) => {
            tooltipMenu[i].classList.remove('tooltip__menu_show');
            headMenuIndex[i].classList.remove('menu__show--active');
        });

        tooltipInner.forEach((elem) => {
            elem.addEventListener('mouseover', (event) => {
                let dataMenu = event.target.parentNode.dataset.menu;
                subcontentWrapper.forEach((el) => {
                    let dataSubmenu = el.dataset.submenu;
                    if (dataMenu === dataSubmenu) {
                        el.classList.add('subcontent__wrapper--active');
                        subcontent.forEach((elem) => {
                            elem.classList.add('subcontent--show');
                            let heightTooltipMenu = el.clientHeight;
                            if (baseHeight < heightTooltipMenu) {
                                tooltipMenu[i].style.height = heightTooltipMenu + 'px';
                            } else {
                                tooltipMenu[i].style.height = baseHeight + 'px';
                                el.style.height = baseHeight + 'px';
                            }
                        });
                    } else {
                        return el.classList.remove('subcontent__wrapper--active');
                    }
                });
            });
        });
    }

    hoverUl.addEventListener('mouseover', (event) => {
        hoverUl.classList.add('nav__container--hover');
    });

    hoverUl.addEventListener('mouseout', (event) => {
        hoverUl.classList.remove('nav__container--hover');
    });
}

if (window.innerWidth <= menuShow) {
    headMenuIndex.forEach((elem) => {
        elem.addEventListener('click', (event) => {
            event.preventDefault();
            elem.classList.toggle('tooltip--arrow');
            let dataMenu = event.target.dataset.menu;
            tooltipMenu.forEach((el) => {
                let dataSubmenu = el.dataset.menu;
                if (dataMenu === dataSubmenu) {
                    el.classList.toggle('tooltip__menu_show');
                }
            });
        });
    });
}
