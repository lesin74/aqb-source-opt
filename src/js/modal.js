'use strict';

const dataSiteKey = '6LeSYE4cAAAAAEan0zacas9FNnVdukFlgmoARuqb';
const bodyModal = document.querySelector('body');
const modal = document.createElement('div');
const pricingCart = document.querySelectorAll('.pricing__card_button');
const regExpEmail = /^[\w-\.\d*]+@[\w\d]+(\.\w{2,4})$/;
bodyModal.appendChild(modal);
modal.classList.add('popup');
let trialType;
let id;
let subscribeToNewsletter = false;

(() => {
    const demoButton = document.getElementById('demo_popup');
    const trialButtons = document.getElementsByClassName('trial_popup');
    const navButtonCall = document.querySelector('.nav__button_call');
    const contactUs = document.querySelector('.contact__us');
    const btnOnlineDemo = document.querySelector('.btn__online_demo');
    const getQuote = document.querySelectorAll('.pricing__quote');

    if (navButtonCall) {
        navButtonCall.addEventListener('click', popupCallbackShow);
    }
    if (contactUs) {
        contactUs.addEventListener('click', popupContactUs);
    }
    if (btnOnlineDemo) {
        btnOnlineDemo.addEventListener('click', popupFeedback);
    }
    if (trialButtons) {
        for (var i = 0; i < trialButtons.length; i++) {
            trialButtons[i].addEventListener('click', (e) => {
                trialPopup(e);
            });
        }
    }
    if (demoButton) {
        demoButton.addEventListener('click', (e) => {
            demoPopup(e);
        });
    }
    if (getQuote) {
        for (var i = 0; i < getQuote.length; i++) {
            getQuote[i].addEventListener('click', (e) => {
                popupGetQuote(e);
            });
        }
    }
})();


//модалка с корзиной товара
pricingCart.forEach((elem) =>{
    elem.addEventListener('click', (event)=>{
        bodyModal.classList.add('body__modal--active');
        modal.classList.add('popup--show', 'popup--show-pricing');
        event.preventDefault();

            //генерация модалки
    modal.innerHTML = `
    ${styles}
    <div class="popup__modal pricing__modal">
        <div class="popup__title pricing__modal_title">Your order<div class="popup--close-pricing"></div></div>
        <div class="pricing__cart_container">
            <div class="pricing__cart_title">
                <div class="pricing__cart_name_product">Product</div>
                <div class="pricing__cart_name_price">Item Price</div>
                <div class="pricing__cart_name_quantity">Quantity</div>
                <div class="pricing__cart_name_amount">Amount</div>
            </div>
            <div class="pricing__cart_product">
                <div class="pricing__cart_product_title">Active Query Builder / for .NET / All in One
                    <div class="pricing__cart_product_subtitle">Standart</div>
                </div>
                <div class="pricing__cart_product_price"><div class="pricing__cart_product_price--mobile">Item Price</div>&nbsp$399</div>
                <div class="pricing__cart_product_quantity">
                    <button type="button" class="pricing__cart_product_btn">+</button>
                    <input type="text" class="pricing__cart_product_inpt" value="5" placeholder="5" />
                    <button type="button" class="pricing__cart_product_btn">-</button>
                </div>
                <div class="pricing__cart_product_amount">$1536</div>
                <button type="button" class="pricing__cart_product_del"></button>
            </div>
            <div class="pricing__cart_product">
            <div class="pricing__cart_product_title">Active Query Builder for .NET - WinForms Source Code<div class="pricing__cart_product_subtitle">Standart</div>
            </div>
            <div class="pricing__cart_product_price"><span class="pricing__cart_product_price--old">$459</span>$399</div>
            <div class="pricing__cart_product_quantity">
                <button type="button" class="pricing__cart_product_btn">+</button>
                <input type="text" class="pricing__cart_product_inpt" value="5" placeholder="5" />
                <button type="button" class="pricing__cart_product_btn">-</button>
            </div>
            <div class="pricing__cart_product_amount">$1536</div>
            <button type="button" class="pricing__cart_product_del"></button>
        </div>
        <!-- если не нужны колонки Valume Discounts и Update to Professional, то просто удаляем весь блок pricing__cart_info --!>
            <div class="pricing__cart_info">
                <div class="pricing__cart_discounts pricing__cart_card">
                    <div class="see__more--btn">See more</div>
                    <div class="pricing__cart_discounts_title">Valume Discounts</div>
                    <div class="pricing__cart_discounts_content">
                        <div class="pricing__cart_discounts_table">
                            <div class="pricing__cart_discounts_table--title">License</div>
                            <div class="pricing__cart_discounts_table--title">Discount</div>
                            <div class="pricing__cart_discounts_table--title">Your Benefit</div>
                        </div>
                        <div class="pricing__cart_discounts_table">
                            <div class="pricing__cart_discounts_table--title pricing__table_fonts">1</div>
                            <div class="pricing__cart_discounts_table--title pricing__table_fonts">0%</div>
                            <div class="pricing__cart_discounts_table--title">$ 0</div>
                        </div>
                        <div class="pricing__cart_discounts_table">
                            <div class="pricing__cart_discounts_table--title pricing__table_fonts">2</div>
                            <div class="pricing__cart_discounts_table--title pricing__table_fonts">22%</div>
                            <div class="pricing__cart_discounts_table--title">$ 88</div>
                        </div>
                        <div class="pricing__cart_discounts_table">
                            <div class="pricing__cart_discounts_table--title pricing__table_fonts">3</div>
                            <div class="pricing__cart_discounts_table--title pricing__table_fonts">30%</div>
                            <div class="pricing__cart_discounts_table--title">$ 360</div>
                        </div>
                        <div class="pricing__cart_discounts_table">
                            <div class="pricing__cart_discounts_table--title pricing__table_fonts">4-7</div>
                            <div class="pricing__cart_discounts_table--title pricing__table_fonts">33%</div>
                            <div class="pricing__cart_discounts_table--title">$ 528 - $ 924</div>
                        </div>
                        <div class="pricing__cart_discounts_table">
                            <div class="pricing__cart_discounts_table--title pricing__table_fonts">8 +</div>
                            <div class="pricing__cart_discounts_table--title pricing__table_fonts">40%</div>
                            <div class="pricing__cart_discounts_table--title">$ 1280 +</div>
                        </div>
                    </div>
                </div>
                <!--
                pricing__cart_update--green - зеленый фон
                pricing__cart_update--blue - голубой фон
                --!>
                <div class="pricing__cart_update pricing__cart_card pricing__cart_update--green">
                    <div class="see__more--btn">See more</div>
                    <div class="pricing__cart_update_title">Update to Professional
                        <div class="pricing__cart_update_title--price">
                            <span class="pricing__new_price">&nbsp;for $719</span><span class="pricing__old_price">$799</span>
                        </div>
                    </div>
                    <div class="pricing__cart_update_list">
                        <ul class="pricing__cart_update_ul">
                            <li class="pricing__cart_update_li">Improves user productivity</li>
                            <li class="pricing__cart_update_li">Saves time on embedding</li>
                        </ul>
                    </div>
                    <div class="pricing__cart_update_footer">
                        <button type="button" class="pricing__cart_update_btn">Update</button>
                        <div class="pricing__cart_update_label">10% discount</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pricing__cart_footer">
            <div class="pricing__cart_footer_discount_code">
                <div class="pricing__cart_footer_discount_code--name">Have a discount code?</div>
                <input class="pricing__cart_footer_discount_code--input" type="text" />
            </div>
            <div class="pricing__cart_footer_discount_total">
                <div class="pricing__cart_footer_discount_total--header">
                    <span class="pricing__cart_footer_discount_total--price">Total: $1536</span>
                    <div class="pricing__cart_footer_discount_total--vat">VAT may be applied
                        <div class="pricing__cart_footer_discount_tooltip--icon">
                            <span class="pricing__cart_footer_discount_tooltip--icon-vat">Perpetual per-developer licensing. 1 year of support and updates. No per-user or per-server royalties or runtime fees. Money back guarantee. VAT may apply.</span>
                        </div>
                    </div>
                </div>
                <button type="button" class="pricing__cart_footer_discount_total--btn">Checkout</button>
            </div>
        </div>
    </div>
    `;

    //открытие\закрытие карточек в модалке корзины
    const seeMore = document.querySelectorAll('.see__more--btn');
    const pricingCartCard = document.querySelectorAll('.pricing__cart_card');
    const discountCode = document.querySelector('.pricing__cart_footer_discount_code--name');
    const discountCodeInput = document.querySelector('.pricing__cart_footer_discount_code--input');

    for(let i = 0; i < pricingCartCard.length; i++){
        if (window.innerWidth >= 441) {
            pricingCartCard[i].addEventListener('click', (event) => {
                pricingCartCard.forEach((e) => {
                    e.classList.remove('pricing__cart_card--open');
                })
                pricingCartCard[i].classList.add('pricing__cart_card--open');
            })
        } else {
            pricingCartCard[i].addEventListener('click', (event) => {
                pricingCartCard[i].classList.toggle('pricing__cart_card--open');
                seeMore[i].innerHTML = "Roll up";
            })
        }
    }

    discountCode.addEventListener('click', () => {
        discountCode.classList.toggle('discount_code_title--open');
        discountCodeInput.classList.toggle('discount_code--input-hidden');
    })

    if (window.innerWidth <= 440) {
        discountCode.classList.add('discount_code_title--open');
        discountCodeInput.classList.add('discount_code--input-hidden');
    } else {
        discountCode.classList.remove('discount_code_title--open');
        discountCodeInput.classList.remove('discount_code--input-hidden');
    }

    seeMore.forEach((e) => {
        e.addEventListener('click', (elem) => {
            let target = elem.target;
            target.classList.toggle('see__more--btn-active');
        })
    })

    //закрытие модалки
    const closeModalPricing = document.querySelector('.popup--close-pricing');
    modal.addEventListener('click', (e) => {
        if (e.target.classList.contains('popup--show')) closeModal();
    });

    closeModalPricing.addEventListener('click', () =>{
        closeModal();
    })
    })
})

const deleteAllOutlineErrors = (isCallback) => {
    const errorText = document.querySelector('.textarea__error');
    const nameSpan = document.querySelector('.input-name-identifier');
    const name = document.querySelector('.popup__input_name');
    const nameLastSpan = document.querySelector('.input-last-name-identifier');
    const workSpan = document.querySelector('.input-work-identifier');
    const work = document.querySelector('.popup__input_work');
    const lastName = document.querySelector('.popup__input_last_name');
    const emailSpan = document.querySelector('.input-email-identifier');
    const email = document.querySelector('.popup__input_email');
    const phoneSpan = document.querySelector('.input-phone-identifier');
    const phone = document.querySelector('.phone');

    errorText.classList.add('popup-hidden');

    nameSpan.classList.add('popup-hidden');
    name.classList.remove('input__error');

    emailSpan.classList.add('popup-hidden');
    email.classList.remove('input__error');

    nameLastSpan.classList.add('popup-hidden');
    lastName.classList.remove('input__error');

    workSpan.classList.add('popup-hidden');
    work.classList.remove('input__error');

    if (isCallback) {
        phoneSpan.classList.add('popup-hidden');
        phone.classList.remove('input__error');
    }
};

const showSuccessText = () => {
    const showSuccessText = document.querySelector('.data__sent');
    showSuccessText.classList.remove('popup-hidden');
};

const addOutlineAndErrorText = (errors) => {
    if (!errors.isEmpty) {
        let errorText = document.querySelector('.textarea__error');
        errorText.classList.remove('popup-hidden');
    } else return;

    if (errors.name) {
        const nameSpan = document.querySelector('.input-name-identifier');
        const name = document.querySelector('.popup__input_name');
        nameSpan.classList.remove('popup-hidden');
        name.classList.add('input__error');
    }
    if (errors.email) {
        const emailSpan = document.querySelector('.input-email-identifier');
        const email = document.querySelector('.popup__input_email');
        emailSpan.classList.remove('popup-hidden');
        email.classList.add('input__error');
    }
    if (errors.phoneNumber) {
        const phoneSpan = document.querySelector('.input-phone-identifier');
        const phone = document.querySelector('.phone');
        phoneSpan.classList.remove('popup-hidden');
        phone.classList.add('input__error');
    }
    if (errors.lastName) {
        const nameLastSpan = document.querySelector('.input-last-name-identifier');
        const lastName = document.querySelector('.popup__input_last_name');
        nameLastSpan.classList.remove('popup-hidden');
        lastName.classList.add('input__error');
    }
    if (errors.work) {
        const workSpan = document.querySelector('.input-work-identifier');
        const work = document.querySelector('.popup__input_work');
        workSpan.classList.remove('popup-hidden');
        work.classList.add('input__error');
    }
};

var sendDataCallbackContactUs = async (isContactUs, token) => {
    const name = document.querySelector('.popup__input_name');
    const email = document.querySelector('.popup__input_email');
    const phoneNumber = isContactUs ? null : document.querySelector('#phone');
    const description = document.querySelector('.dropdown__description');
    let phoneNumberCountry =
        '+' +
        document.querySelectorAll('[aria-selected="true"]')[0]?.getAttribute('data-dial-code');

    try {
        let phoneWithCode = phoneNumber?.value;
        if (!phoneNumber?.value?.includes('+')) phoneWithCode = phoneNumberCountry + phoneWithCode;

        dataLayer.push({
            event: 'contact',
            contactType: isContactUs ? 'contact' : 'callback',
            cvalue: 0.5,
        });

        let dataString = `{
            "g-recaptcha-response": "${token}",
            "name": "${name?.value}",
            "email": "${email?.value}",
            "message": "${description?.value}",
            ${!isContactUs ? '' : `"subscribeToNewsletter": ${subscribeToNewsletter},`}
            ${isContactUs ? '' : `"phone": "${phoneWithCode}",`}
            "callbackRequest": ${isContactUs !== true}
        }`;
        let data = JSON.parse(dataString);
        await $.post('https://www.activequerybuilder.com/member/submitForms.php', data);
        showSuccessText();
    } catch (e) {
        console.log(e);
    }
};

function popupCallbackShow(event) {
    bodyModal.classList.add('body__modal--active');
    modal.classList.add('popup--show');
    event.preventDefault();

    //генерация модалки
    modal.innerHTML = `
    ${styles}
    <div class="popup__modal" id="callbackPopUp">
        <div class="popup__title">Request a callback</div>
        <div class="data__sent popup-hidden">Thank you! We will contact you as soon as possible.</div>
        <form class="popup__form">
            <input type="text" class="popup__input_name" name="name" placeholder="Name" />
            <span class="span__error input-name-identifier popup-hidden">Please type your name</span>
            <input type="text" class="popup__input_email" name="email" placeholder="Your e-mail" />
            <span class="span__error input-email-identifier popup-hidden">Please type your email address</span>
            <div class="phone">
                <input class="popup__input_email" style="width: 100%" id="phone" type="tel">
            </div>
            <span class="span__error input-phone-identifier popup-hidden">Please type your phone number</span>
            <textarea
                class="dropdown__description"
                name="message"
                id=""
                cols="30"
                rows="10"
                placeholder="What time is convenient to call you back? Anything else?..."
            ></textarea>
            <div class="textarea__error popup-hidden">Please fill in all required fields</div>
            <button type="button" class="popup__btn" id="callBack">Please call me back</button>
        </form>
        <span class="popup__subtitle">We'll be glad to help you.</span>
    </div>
    <div id='recaptcha' class="g-recaptcha"
        data-sitekey="${dataSiteKey}"
        data-callback="onSubmit"
        data-size="invisible"
    >
    </div>
    `;
    grecaptcha.render('recaptcha');

    loadTelInput();

    const submitButton = document.getElementById('callBack');

    submitButton.addEventListener('click', (e) => {
        if (formHasNoErrors()) grecaptcha.execute();
    });

    //закрытие модалки
    modal.addEventListener('click', (e) => {
        if (e.target.classList.contains('popup--show')) closeModal();
    });
}

function popupContactUs(event) {
    bodyModal.classList.add('body__modal--active');
    modal.classList.add('popup--show');
    event.preventDefault();

    //генерация модалки
    modal.innerHTML = `
    ${styles}
    <div class="popup__modal" id="contactUsPopUp">
        <div class="popup__title">Contact us</div>
        <div class="data__sent popup-hidden">Thank you! We will contact you as soon as possible.</div>
        <form action="" class="popup__form">
            <input type="text" class="popup__input_name" name="name" placeholder="Name" />
            <span class="span__error input-name-identifier popup-hidden">Please type your name</span>
            <input type="text" class="popup__input_email email__contact_us" name="email" placeholder="Email" />
            <span class="span__error input-email-identifier popup-hidden">Please type your email address</span>
            <textarea
                class="dropdown__description"
                name="message"
                id=""
                cols="30"
                rows="10"
                placeholder="How can we help you?"
            ></textarea>
            <div class="popup__checkbox">
                <input type="checkbox" class="popup-checkbox" id="label" name="label" value="yes">
                <label for="label" id="subscribe">Send me Active Query Builder news from time to time</label>
            </div>
            <div class="textarea__error popup-hidden">Please fill in all the required fields</div>
            <button type="button" class="popup__btn" id="contactUs">Contact me</button>
        </form>
        <span class="popup__subtitle">We'll reach out to you in hours</span>
    </div>
    <div id='recaptcha' class="g-recaptcha"
        data-sitekey="${dataSiteKey}"
        data-callback="onSubmit"
        data-size="invisible"
    >
    </div>
    `;
    grecaptcha.render('recaptcha');
    const subscribeButton = document.getElementById('subscribe');
    const submitButton = document.getElementById('contactUs');

    submitButton.addEventListener('click', (e) => {
        if (formHasNoErrors(true)) grecaptcha.execute();
    });

    subscribeButton.addEventListener('click', (e) => {
        subscribeToNewsletter = !subscribeToNewsletter;
    });

    //закрытие модалки
    modal.addEventListener('click', (e) => {
        if (e.target.classList.contains('popup--show')) closeModal();
    });
}

function popupGetQuote(event) {
    bodyModal.classList.add('body__modal--active');
    modal.classList.add('popup--show', 'popup--show-pricing');
    event.preventDefault();

    //генерация модалки
    modal.innerHTML = `
    ${styles}
    <div class="popup__modal" id="contactUsPopUp">
        <div class="popup__title">Get Quote<div class="popup--close-quote"></div></div>
        <div class="data__sent popup-hidden">Thank you! We will contact you as soon as possible.</div>
        <form action="" class="popup__form">
            <label class="popup__input_label">Company Name</label>
                <input type="text" class="popup__input_name" name="name" placeholder="" />
            <span class="span__error input-name-identifier popup-hidden">Please type your name</span>
            <label class="popup__input_label">First and Last Name</label>
                <input type="text" class="popup__input_last_name" name="name" placeholder="" />
            <span class="span__error input-last-name-identifier popup-hidden">Please type your First and Last Name</span>
            <label class="popup__input_label">Work Position</label>
                <input type="text" class="popup__input_work" name="name" placeholder="" />
            <span class="span__error input-work-identifier popup-hidden">Work Position</span>
            <label class="popup__input_label">Email address *</label>
                <input type="text" class="popup__input_email email__contact_us popup__input_getquote" name="email" placeholder="" />
            <span class="span__error input-email-identifier popup-hidden">Please type your email address</span>
            <div class="support__checkbox">
                <input type="checkbox" class="support__checkbox_input" id="label" name="label" value="yes" />
                <label for="label">I agree to the processing of my personal data and the Privacy Policy.</label>
            </div>
            <div class="textarea__error popup-hidden">Please fill in all the required fields</div>
            <button type="button" class="popup__btn popup__btn_getquote" id="getQuote">Submit Request</button>
        </form>
    </div>
    <div id='recaptcha' class="g-recaptcha"
        data-sitekey="${dataSiteKey}"
        data-callback="onSubmit"
        data-size="invisible"
    >
    </div>
    `;
    grecaptcha.render('recaptcha');
    const submitButton = document.getElementById('getQuote');

    submitButton.addEventListener('click', (e) => {
//        if (formHasNoErrors(true)) grecaptcha.execute();
            modal.innerHTML = `
            ${styles}
            <div class="popup__modal">
                <div class="popup__title popup__title--quote">Thank You<div class="popup--close-quote"></div></div>
                <div class="popup__content">The quote was sent to your email.<br>
                You can download it <a href="#" class="download__pdf">here</a> in pdf format.</div>
                <img src="./img/download-pdf.png" class="popup__content--bg" alt="" />
            </div>
            `;
        const closeModalQuote = document.querySelector('.popup--close-quote');
        closeModalQuote.addEventListener('click', () =>{
            closeModal();
        })
    });

    //закрытие модалки
    const closeModalQuote = document.querySelector('.popup--close-quote');
    modal.addEventListener('click', (e) => {
        if (e.target.classList.contains('popup--show')) closeModal();
    });

    closeModalQuote.addEventListener('click', () =>{
        closeModal();
    })
}

function popupFeedback(event) {
    bodyModal.classList.add('body__modal--active');
    modal.classList.add('popup--show', 'popup__feedback');
    event.preventDefault();

    //генерация модалки
    modal.innerHTML = `
        ${styles}
        <div class="popup__modal modal__feedback">
        <div class="feedback__step1--active">
            <div class="popup__title">Feedback</div>
                <div class="data__sent popup-hidden">Thank you! We will contact you as soon as possible.</div>
                    <div class="feedback__subtitle">
                        How would you rate your experience with Active Query Builder?
                    </div>
                    <div class="feedback__raiting">
                    <div class="rating">
                    <input type="radio" id="star-1" name="rating" value="1">
                    <label for="star-1" class="feedback__raiting_label" title="Оценка «1»"></label>
                    <input type="radio" id="star-2" name="rating" value="2">
                    <label for="star-2" class="feedback__raiting_label" title="Оценка «2»"></label>
                    <input type="radio" id="star-3" name="rating" value="3">
                    <label for="star-3" class="feedback__raiting_label" title="Оценка «3»"></label>
                    <input type="radio" id="star-4" name="rating" value="4">
                    <label for="star-4" class="feedback__raiting_label" title="Оценка «4»"></label>
                    <input type="radio" id="star-5" name="rating" value="5">
                    <label for="star-5" class="feedback__raiting_label" title="Оценка «5»"></label>
                  
                  </div>
                </div>
            </div>
            <div class="feedback__step2">
                <form action="">
                <div class="feedback__subtitle feedback__subtitle--step2">Why did you put such a rating, share it with us?</div>
                    <textarea 
                     class="feedback__msg"
                     id="feedback-msg"
                     name="feedback-msg"
                     rows="4"
                     cols="50"
                     placeholder="Write your message here"></textarea>
                     <button class="btn__feedback" type="button">Send</button>
                </form>
            </div>
            <div class="feedback__step3">
            <div class="feedback__subtitle feedback__subtitle--step3">Thank you for your feedback</div>
            <img src="./img/feedback-img.png" alt="" />
            </div>
        </div>
        `;
    const feedbackStep1 = document.querySelector('.feedback__step1--active');
    const feedbackStep2 = document.querySelector('.feedback__step2');
    const feedbackStep3 = document.querySelector('.feedback__step3');
    const feedbackRaitingLabel = document.querySelectorAll('.feedback__raiting_label');
    const btnFeedback = document.querySelector('.btn__feedback');
    feedbackRaitingLabel.forEach((item) => {
        item.addEventListener('click', () => {
            feedbackStep1.classList.add('feedback__step1--done');
            feedbackStep2.classList.add('feedback__step2--active');
        });
    });

    btnFeedback.addEventListener('click', () => {
        feedbackStep2.classList.add('feedback__step2--done');
        feedbackStep3.classList.add('feedback__step3--active');
    });

    //закрытие модалки
    modal.addEventListener('click', (e) => {
        if (e.target.classList.contains('popup--show')) closeModal();
    });
}

function trialPopup(event) {
    bodyModal.classList.add('body__modal--active');
    modal.classList.add('popup--show');
    initializeDemoTrialModal(true);
    loadTelInput();
    event.preventDefault();

    let title = document.getElementsByClassName('popup__title')[0];
    let description = document.getElementsByClassName('popup__description')[0];
    let subtitle = document.getElementsByClassName('popup__subtitle')[0];
    let popupButton = document.getElementsByClassName('popup__btn')[0];

    id = 'trial';
    trialType = event.target.id;
    title.innerHTML = 'AQB Trial Version Download';
    description.innerHTML = "You're about to download the trial version of Active Query Builder.";
    subtitle.innerHTML =
        "The trial version of Active Query Builder doesn't have time or functional limitations except for the addition of random aliases for columns to the query text.";
    popupButton.innerHTML = 'Download now';

    addModalListeners('trialDownload');
}

function demoPopup(event) {
    bodyModal.classList.add('body__modal--active');
    modal.classList.add('popup--show');
    initializeDemoTrialModal();
    loadTelInput();
    event.preventDefault();

    let title = document.getElementsByClassName('popup__title')[0];
    let description = document.getElementsByClassName('popup__description')[0];
    let subtitle = document.getElementsByClassName('popup__subtitle')[0];
    let popupButton = document.getElementsByClassName('popup__btn')[0];

    id = 'demo';
    title.innerHTML = 'Demo App Download';
    description.innerHTML =
        "You're about to download the Desktop Active Query Builder demo application.";
    subtitle.innerHTML =
        "Active Query Builder Demo doesn't have time or functional limitations except for the addition of random aliases for columns to the query text.";
    popupButton.innerHTML = 'Download now';

    addModalListeners('demoDownload');
}

function addModalListeners(submitButtonElementId) {
    const submitButton = document.getElementById(submitButtonElementId);
    const subscribeButton = document.getElementById('subscribe');

    subscribeButton.addEventListener('click', (e) => {
        subscribeToNewsletter = !subscribeToNewsletter;
    });
    submitButton.addEventListener('click', (e) => {
        if (formHasNoErrors()) grecaptcha.execute();
    });

    //закрытие модалки
    modal.addEventListener('click', (e) => {
        if (e.target.classList.contains('popup--show')) {
            closeModal();
            modal.id = '';
            id = '';
            trialType = '';
        }
    });
}

const formHasNoErrors = (isContactUs) => {
    deleteAllOutlineErrors(!isContactUs);

    let errors = { isEmpty: true };
    const name = document.querySelector('.popup__input_name');
    const email = document.querySelector('.popup__input_email');
    const lastName = document.querySelector('.popup__input_last_name');
    const work = document.querySelector('.popup__input_work');
    const phoneNumber = isContactUs ? null : document.querySelector('#phone');

    if (phoneNumber !== null && (!phoneNumber.value || phoneNumber.value.length < 1))
        errors = Object.assign(errors, { phoneNumber: true, isEmpty: false });
    if (!name?.value || name?.value.length < 1)
        errors = Object.assign(errors, { name: true, isEmpty: false });
    if (!lastName?.value || lastName?.value.length < 1)
        errors = Object.assign(errors, { lastName: true, isEmpty: false });
    if (!work?.value || work?.value.length < 1)
        errors = Object.assign(errors, { work: true, isEmpty: false });
    if (!email?.value || email?.value.length < 1 || !regExpEmail.test(email?.value))
        errors = Object.assign(errors, { email: true, isEmpty: false });

    addOutlineAndErrorText(errors);
    return errors.isEmpty;
};

var sendFormDataToCrisp = function (token) {
    const fName = document.querySelector('.popup__input_name')?.value;
    const fEmail = document.querySelector('.popup__input_email')?.value;
    const fPhone = document.querySelector('#phone')?.value;
    const fSubscribe = subscribeToNewsletter;
    var fReferer = trialType;
    var fFormId = id;

    var fTrialRequest = fFormId === 'trial';
    var fDemoRequest = fFormId === 'demo';
    var fDemoType = '';
    var fDownloadURL = 'https://www.activequerybuilder.com/files/';

    var downloadTypes = {
        WinForms: {
            event: 'aqb.net',
            url: 'aqbnet3_trial.zip',
            cvalue: 10,
        },
        WPF: {
            event: 'aqb.net',
            url: 'aqbnet3_trial.zip',
            cvalue: 10,
        },
        'ASP.NET': {
            event: 'aqb.net',
            url: 'aqb_aspnet3_trial.zip',
            cvalue: 10,
        },
        Java: {
            event: 'aqb.net',
            url: 'aqbjava_trial.zip',
            cvalue: 1,
        },
        Delphi: {
            event: 'aqb.net',
            url: 'aqb_trial.zip',
            cvalue: 5,
        },
        ActiveX: {
            event: 'aqb.net',
            url: 'aqbx_trial.zip',
            cvalue: 1,
        },
        Demo: {
            event: 'aqb.demo',
            url: 'aqb_demo.zip',
            cvalue: 1,
        },
    };

    var getTrialType = () => {
        switch (fReferer) {
            case '1':
                fDemoType = 'WinForms';
                break;
            case '2':
                fDemoType = 'WPF';
                break;
            case '3':
                fDemoType = 'ASP.NET';
                break;
            case '4':
                fDemoType = 'Delphi';
                break;
            case '5':
                fDemoType = 'Java';
                break;
            case '6':
                fDemoType = 'ActiveX';
                break;
            default:
                fDemoType = 'trial';
                break;
        }
    };
    var downloadFile = (id) => {
        if (id === 'form343028083') {
            fDemoType = 'Demo';
        } else {
            getTrialType();
        }

        fDownloadURL = fDownloadURL + downloadTypes[fDemoType].url;
        var iframe = document.createElement('iframe');
        iframe.src = fDownloadURL;
        document.body.appendChild(iframe);

        dataLayer.push({
            download: downloadTypes[fDemoType].event,
            event: 'download',
            cvalue: downloadTypes[fDemoType].cvalue,
        });

        setTimeout(function () {
            iframe.remove();
        }, 1000);
    };

    if (fTrialRequest) {
        downloadFile('form352673559');
    }

    if (fDemoRequest) {
        downloadFile('form343028083');
    }

    $.post('https://www.activequerybuilder.com/member/submitForms.php', {
        'g-recaptcha-response': token,
        name: fName,
        email: fEmail,
        phone: fPhone,
        subscribeToNewsletter: fSubscribe,
        demoType: fDemoType,
    });

    let fSuccessMessage = '';

    if (fDemoType != '') {
        fSuccessMessage =
            'Thank you for downloading!<br/> If your download doesn\'t start, <a href="' +
            fDownloadURL +
            '" style="color:#4B0082"><span style="color:#4B0082">click here</span></a>.';
    }
    const showSuccessText = document.querySelector('.data__sent');
    showSuccessText.innerHTML = fSuccessMessage;
    showSuccessText.classList.remove('popup-hidden');
};

const loadTelInput = () => {
    var input = modal.querySelector('#phone');
    intlTelInput(input, {
        initialCountry: 'auto',
        geoIpLookup: async function (callback) {
            const response = await $.get(
                'https://ipinfo.io/?token=fe50c79100221a',
                function () {},
                'jsonp',
            );

            const countryCode = response && response.country ? response.country : 'us';
            callback(countryCode);
        },
        utilsScript:
            'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/js/utils.min.js',
    });
};

var onSubmit = (token) => {
    let contactUs = document.getElementById('contactUsPopUp');
    let callback = document.getElementById('callbackPopUp');
    let demoTrial = document.getElementById('demoTrialPopUp');
    if (contactUs) {
        sendDataCallbackContactUs(true, token);
    } else if (callback) {
        sendDataCallbackContactUs(false, token);
    } else if (demoTrial) {
        sendFormDataToCrisp(token);
    }
};

const closeModal = () => {
    bodyModal.classList.remove('body__modal--active');
    modal.classList.remove('popup--show');
    modal.innerHTML = '';
    subscribeToNewsletter = false;
};

const initializeDemoTrialModal = (isTrial) => {
    modal.id = 'demoTrialPopUp';
    modal.innerHTML = `
    ${styles}
    <div class="popup__modal">
        <div class="popup__title" id="demoTrialTitle"></div>
        <div class="popup__description"></div>
        <div class="data__sent popup-hidden">Thank you! Data has been sent successfully.</div>
        <form class="popup__form">
            <input type="text" class="popup__input_name" id="formName" name="name" placeholder="Name" />
            <span class="span__error input-name-identifier popup-hidden">Please type your name</span>
            <input type="text" class="popup__input_email" name="email" placeholder="Your e-mail" />
            <span class="span__error input-email-identifier popup-hidden">Please type your email address</span>
            <input type="text" class="popup__input_email" name="company" placeholder="Company" />
            <span class="span__error input-company-identifier popup-hidden">Please type your company name</span>
            <div class="phone">
                <input class="popup__input_email" style="width: 100%" id="phone" type="tel">
            </div>
            <span class="span__error input-phone-identifier popup-hidden">Please fill your phone number</span>
            <div class="popup__checkbox">
                <input type="checkbox" class="popup-checkbox" id="label" name="label" value="yes">
                <label for="label" id="subscribe">Send me Active Query Builder news from time to time</label>
            </div>
            <div class="textarea__error popup-hidden">Please fill in all the required fields</div>
            <button type="button" class="popup__btn" id="${
                isTrial ? 'trialDownload' : 'demoDownload'
            }"></button>
        </form>
        <span class="popup__subtitle"></span>
    </div>
    <div id='recaptcha' class="g-recaptcha"
        data-sitekey="${dataSiteKey}"
        data-callback="onSubmit"
        data-size="invisible"
    >
    </div>
    `;
    grecaptcha.render('recaptcha');
};

const styles = `<style type="text/css">

@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');

.popup {
    opacity: 0;
    overflow: auto;
}
.popup__description{
    text-align: center;
    margin-bottom: 20px;
}
.popup--show {
    width: 100%;
    height: 100%;
    position: fixed;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, .6);
    opacity: 1;
    z-index: 300;
    transition: opacity 0.3s ease-in-out;
}
.popup--show:before {
    content: '';
    position: fixed;
    width: 24px;
    height: 24px;
    top: 15px;
    right: 20px;
    background-image: url('../img/close-modal.png');
    background-repeat: no-repeat;
    background-size: contain;
    cursor: pointer;
}
.popup__modal {
    width: 100%;
    max-width: 540px;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 40px 45px;
    margin: 65px auto;
    background-color: #fff;
    position: relative;
    top: 0;
    left: auto;
    right: auto;
    transition: all 0.3s ease-in-out;
}
.modal__feedback{
    max-width: 550px;
    margin: auto;
    top: 50%;
    transform: translate(0, -50%);
}
.feedback__subtitle{
    font-size: 16px;
    line-height: 19px;
    font-weight: 300;
}
.popup__title {
    text-align: center;
    margin-bottom: 20px;
    font-family: 'Roboto-Bold', sans-serif;
    font-weight: 600;
    font-size: 36px;
    line-height: 1.23;
}
.popup__content {
    font-family: 'Roboto-Light', sans-serif;
    font-size: 18px;
    line-height: 27px;
    font-weight: 300;
    text-align: center;
    color: black;
}
.download__pdf {
    font-family: 'Roboto-Light', sans-serif;
    font-size: 18px;
    line-height: 27px;
    font-weight: 300;
    text-decoration: underline #7bc866;
    text-underline-offset: 3px;
    -webkit-text-underline-offset: 3px;
    -ms-text-underline-offset: 3px;
    color: black;
}
.popup__content--bg {
    display: block;
    margin-top: 35px;
}
.popup__form {
    width: 100%;
    display: flex;
    flex-direction: column;
}
.popup__form > textarea {
    height: 102px;
    padding: 18px;
    margin-top: 25px;
    border: solid #c9c9c9 1px;
    border-radius: 5px;
    resize: vertical;
    font-family: 'Roboto-Regular';
    font-size: 16px;
    font-weight: 400;
    color: #000;
}
.popup__input_name,
.popup__input_email,
.popup__input_last_name,
.popup__input_work {
    padding: 17px 58px;
    border: solid #c9c9c9 1px;
    border-radius: 5px;
    font-family: 'Roboto-Regular';
    font-size: 14px;
    font-weight: 400;
    color: #000;
}
.popup__input_label {
    margin-bottom: 12px;
    font-family: 'Roboto-Regular', sans-serif;
    font-size: 16px;
    line-height: 16px;
    font-weight: 400;
    color: #000;
}
.popup__input_label:not(:first-child){
    margin-top: 22px;
}
.popup__input_name,
.popup__input_last_name,
.popup__input_work {
    margin-bottom: 8px;
}
.popup__input_email {
    margin-top: 20px;
    margin-bottom: 8px;
}
.popup__input_getquote{
    margin-top: unset;
}
.popup__btn {
    display: block;
    width: 100%;
    height: 54px;
    margin-top: 25px;
    background-color: #000;
    border: none;
    border-radius: 5px;
    font-family: 'Roboto-Bold', sans-serif;
    font-size: 16px;
    font-weight: 700;
    color: #fff;
    cursor: pointer;
}
.popup__btn_getquote {
    background-color: #7bc866;
}
.popup__subtitle {
    display: block;
    margin-top: 20px;
    font-family: 'Roboto-Light';
    font-size: 15px;
    line-height: 1.55;
    font-weight: 300;
}
.popup__checkbox {
    display: flex;
    align-items: center;
    margin-top: 20px;
    cursor: pointer;
}
.popup__checkbox > label {
    font-family: 'Roboto-Light';
    font-size: 15px;
    font-weight: 300;
    cursor: pointer;
}
.dropdown {
    display: flex;
    position: relative;
}
.dropdown__country {
    height: 57px;
    border: solid #c9c9c9 1px;
    border-radius: 5px;
    font-family: 'Roboto-Regular';
    cursor: pointer;
    margin-bottom: 8px;
}
.dropdown__wrapper {
    max-width: 150px;
    display: flex;
    align-items: center;
}
.dropdown__items {
    display: none;
}
.dropdown__items--active {
    width: 100%;
    max-height: 212px;
    max-width: 400px;
    display: block;
    position: absolute;
    border: solid #eee 1px;
    border-radius: 7px;
    background-color: #fff;
    top: calc(100% + 5px);
    overflow: auto;
}
.dropdown__item {
    display: flex;
    justify-content: space-between;
    font-size: 14px;
    padding: 10px 16px 10px 8px;
    cursor: pointer;
}
.dropdown__item:hover {
    background-color: #eee;
}
.dropdown__item_label {
    cursor: pointer;
}
.dropdown__country_number {
    width: 100%;
    margin-left: 10px;
    margin-top: 2px;
    border: none;
    border-top-right-radius: 7px;
    border-bottom-right-radius: 7px;
    background-color: transparent;
}
.dropdown__country_number::placeholder {
    font-family: 'Roboto-Regular';
    font-size: 16px;
    font-weight: 400;
}
.dropdown__name_country {
    margin-left: 15px;
    text-transform: uppercase;
    cursor: pointer;
}
.dropdown__code_phone {
    margin-left: 10px;
    cursor: pointer;
}
.input__error {
    border: solid red 1px;
}
.marginphoneblock {
    margin-top: 20px;
}
.span__error {
    font-family: 'Roboto-Regular';
    font-size: 13px;
    font-weight: 400;
    color: red;
}
.textarea__error {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 20px;
    padding: 20px 30px;
    text-align: center;
    background-color: #f95d51;
    font-family: 'Roboto-Light';
    font-size: 20px;
    line-height: 1.55;
    font-weight: 300;
    color: #fff;
}
.data__sent {
    width: 100%;
    align-items: center;
    justify-content: center;
    margin-bottom: 20px;
    padding: 15px 35px;
    text-align: center;
    background-color: #62c584;
    font-family: 'Roboto-Light';
    font-size: 20px;
    line-height: 1.55;
    font-weight: 300;
    color: #fff;
}
.email__contact_us {
    margin-bottom: 8px;
}
.popup-checkbox {
    position: absolute;
    z-index: -1;
    opacity: 0;
}
.popup-checkbox + label {
    display: inline-flex;
    align-items: center;
    user-select: none;
}
.popup-checkbox + label::before {
    content: '';
    display: inline-block;
    width: 18px;
    height: 18px;
    flex-shrink: 0;
    flex-grow: 0;
    border: 2px solid #000;
    margin-right: 10px;
    background-repeat: no-repeat;
    background-position: center center;
    background-size: 50% 50%;
}
.popup-checkbox:checked + label::before {
    background-image: url("../img/checked.png");
    background-repeat: no-repeat;
    background-size: auto;
}
.popup-hidden {
    display: none;
}
.iti__flag {
    background-image: url("../img/vendor/intl_tel_input/flags.png");
}
@media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
    .iti__flag {
        background-image: url("../img/vendor/intl_tel_input/flags@2x.png");
   }
}
.phone {
    width: 100%;
    margin-top: 20px;
    margin-bottom: 8px;
}
.phone > div {
    width: 100%;
}
.feedback__raiting{
    display: flex;
    justify-content: center;
}

.rating {
    display: flex;
    width: 100%;
    height: 70px;
    margin-top: 30px;
    flex-direction: row-reverse;
    align-items: center;
    justify-content: center;
    position: relative;
    right: 20px;
  }
  
  .rating:not(:checked)>input {
    display: none;
  }
  
  .rating:not(:checked)>label {
    width: 65px;
    cursor: pointer;
    font-size: 65px;
    color: lightgrey;
    text-align: center;
    line-height: 1;
  }
  
  .rating > label:not(:last-child) {
    margin-left: 20px;
    padding-left: 20px;
  }

  .rating > label:last-child {
    padding-left: 20px;
  }

  .rating:not(:checked)>label:before {
    content: '★';
  }
  
  .rating>input:checked~label {
    color: gold;
  }
  
  .rating:not(:checked)>label:hover,
  .rating:not(:checked)>label:hover~label {
    color: gold;
  }
  
  .rating>input:checked+label:hover,
  .rating>input:checked+label:hover~label,
  .rating>input:checked~label:hover,
  .rating>input:checked~label:hover~label,
  .rating>label:hover~input:checked~label {
    color: gold;
  }


.feedback__subtitle--step2{
    text-align: center;
    margin-bottom: 20px;
}

.feedback__msg{
    width: 100%;
    max-width: 450px;
    max-height: 220px;
    min-height: 60px;
    padding: 20px;
    border: solid #c9c9c9 1px;
    border-radius: 5px;
    resize: vertical;
}

.feedback__msg::placeholder{
    color: rgba(0, 0, 0, 0.5);
    font-size: 16px;
    line-height: 16px;
    font-weight: 300;
}

.btn__feedback {
    width: 100%;
    height: 50px;
    margin-top: 20px;
    background: #7BC866;
    border-radius: 5px;
    border: none;
    color: white;
    font-size: 16px;
    line-height: 19px;
    font-weight: 500;
    cursor: pointer;
  }

.feedback__step1--active{
    display: block;
    padding: 20px 5px 33px;
    text-align: center;
}

.feedback__subtitle--step3{
    margin-bottom: 16px;
    text-align: center;
    font-size: 22px;
    line-height: 28px;
    font-weight: bold;
    color: #000;
}

.feedback__step2,
.feedback__step3{
    display: none;
}

.feedback__step2--active{
    display: block;
    padding: 10px 5px;
}

.feedback__step3--active{
    display: block;
}

.feedback__step1--done,
.feedback__step2--done{
    display: none;
}

@media screen and (max-width: 1200px){
    .popup__modal{
        max-width: 460px;
    }

    .feedback__msg{
        max-width: 410px;
    }
}

@media screen and (max-width: 639px){
.popup__modal{
    padding: 20px;
}

.popup__input_email,
.popup__input_phone,
.popup__input_name{
    padding: 17px 14px;
    background-color: #51FADD;
}

.popup__form > textarea{
    padding: 10px;
}

.popup__title{
    text-align: center;
    font-size: 28px;
}

.popup__subtitle{
    font-size: 12px;
}

.textarea__error{
    padding: 20px 15px;
    font-size: 16px;
}

.data__sent{
    padding: 15px 25px;
    font-size: 16px;
}

.popup__checkbox > label{
    font-size: 12px;
    line-height: 1.45;
}
}

@media screen and (max-width: 560px){
.popup__modal{
    max-width: 100%;
    margin: 0;
    padding-top: 70px;
    position: fixed;
    right: 0;
    left: 0;
    top: 0;
    bottom: 0;
    overflow: auto;
}

.popup--show::before{
    width: 100%;
    padding: 13px;
    background-image: unset;
    background-color: black;
    top: unset;
    right: unset;
    z-index: 2;
}

.popup--show::after{
    width: 18px;
    height: 23px;
    content: '';
    position: fixed;
    top: 16px;
    right: 20px;
    background-image: url('../img/close-modal.png');
    background-repeat: no-repeat;
    background-size: contain;
    z-index: 2;
    cursor: pointer;
}

.popup__modal.modal__feedback{
    display: flex;
    justify-content: center;
    padding: 30px;
    transform: unset;
}

.popup__feedback::before{
    background-image: url('../img/close-feedback.png');
    background-size: 24px;
    background-color: transparent;
    background-position: center right;
    box-sizing: border-box;
    top: 15px;
    right: 15px;
    z-index: 3;
}

.feedback__msg{
    width: 100%;
    max-width: 100%;
}
.feedback__step1--active .popup__title{
    font-size: 36px;
}
}

@media screen and (max-width: 480px){
    .feedback__raiting{
        font-size: 50px;
    }

    .rating:not(:checked) > label {
        width: 50px;
        font-size: 50px;
      }
}

@media screen and (max-width: 400px){
    .rating > label:not(:last-child) {
        margin-left: 10px;
        padding-left: 10px;
      }

      .rating > label:last-child {
        padding-left: 10px;
      }

      .rating{
          right: 10px;
      }
}

@media screen and (max-width: 375px){
    .feedback__raiting{
        font-size: 50px;
    }
}

@media screen and (max-width: 359px){
    .feedback__raiting{
        font-size: 45px;
    }

    .rating:not(:checked) > label {
        width: 45px;
        font-size: 45px;
      }

    .rating > label:not(:last-child) {
        margin-left: 5px;
        padding-left: 5px;
      }

      .rating > label:last-child {
        padding-left: 5px;
      }

      .rating{
          right: 5px;
      }
}

</style>`;
