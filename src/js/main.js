'use strict';
import { sliders } from './sliders';

const body = document.querySelector('body');
const nav = document.querySelector('#nav');
const videoPopup = document.querySelector('.header__board_video');
const headerPointer = document.querySelector('.header__pointer');
const youtubeVideo = document.querySelector('.header__iframe');
let div = document.createElement('div');
const tabsTitleItem = document.querySelectorAll('.tabs__title_item');
const tabsContent = document.querySelectorAll('.tabs__content');
const featuresBuildingTabs = document.querySelectorAll('.features__building_content');
const navHeight = 76;
const leftInfo = document.querySelector('.left__info');
const rightInfo = document.querySelector('.right__info');
const infoContent = document.querySelector('.info__content > picture > img');
const helpBg = document.querySelector('.help');
const started = document.querySelector('.started');
const tabsSelectItem = document.querySelector('.tabs__select_item');
const levelTabsTitle = document.querySelectorAll('.level__tabs_title');
const levelTabsItem = document.querySelectorAll('.level__tabs_item');
const tabsBlockTitle = document.querySelectorAll('.tabs__block_title');
const tabsBlockContent = document.querySelectorAll('.tabs__block_content');

//смена бэграунда хедера при скролле
if (headerPointer) {
    window.addEventListener('scroll', (event) => {
        if (navHeight >= headerPointer.getBoundingClientRect().top) {
            nav.classList.add('nav__bg--active');
        } else {
            nav.classList.remove('nav__bg--active');
        }
    });
} else {
    nav.classList.add('nav__bg--active');
}

//табы на странице Active Query Builder for End Users
for (let i = 0; i < levelTabsTitle.length; i++) {
    if (levelTabsItem[i].classList.contains('level__tabs_item--active')) {
        levelTabsItem[i].style.maxHeight = levelTabsItem[i].scrollHeight + 'px';
    }
    levelTabsTitle[i].addEventListener('click', (title) => {
        levelTabsTitle[i].classList.toggle('level__tabs_title--active');
        levelTabsItem[i].classList.toggle('level__tabs_item--active');
        if (levelTabsItem[i].classList.contains('level__tabs_item--active')) {
            levelTabsItem[i].style.maxHeight = levelTabsItem[i].scrollHeight + 'px';
        } else {
            levelTabsItem[i].style.maxHeight = null;
        }
    });
}

sliders();

tabsTitleItem.forEach((item) => {
    item.addEventListener('click', (event) => {
        tabsTitleItem.forEach((e) => {
            e.classList.remove('tabs__title_item--active');
        });
        item.classList.add('tabs__title_item--active');
        tabsContent.forEach((content) => {
            if (item.dataset.tabs == content.dataset.tabs) {
                tabsContent.forEach((e) => {
                    e.classList.remove('tabs__content--active');
                });
                content.classList.add('tabs__content--active');
            }
        });
    });
});

if (tabsSelectItem) {
    tabsSelectItem.addEventListener('change', (event) => {
        tabsContent.forEach((content) => {
            if (tabsSelectItem.value == content.dataset.tabs) {
                tabsContent.forEach((e) => {
                    e.classList.remove('tabs__content--active');
                });
                content.classList.add('tabs__content--active');
            }
        });
    });
}


//табы на странице Features
featuresBuildingTabs.forEach((el) => {
    let featuresLi = el.querySelectorAll('.features__building_list-li');
    let featuresScreen = el.querySelectorAll('.features__building_img-screen');
    let featuresScreenMobile = el.querySelectorAll('.features__building_img-screen');
    let featuresBuildingItems = el.querySelectorAll('.features__building_items');
    let featuresBuildingItem = el.querySelectorAll('.features__building_item');
    let featuresBuildingRow = el.querySelectorAll('.features__building_row');
        for(let i = 0; i < featuresLi.length; i++){
            featuresLi[0].click();
            featuresLi[i].onclick = () => {
                featuresLi.forEach((el) => {el.classList.remove('features__building_list-li--active')});
                featuresScreen.forEach((el) => {el.classList.remove('features__building_img--active')});
                featuresLi[i].classList.add('features__building_list-li--active');
                featuresScreen[i].classList.add('features__building_img--active');
            }
        }
        for(let i = 0; i < featuresBuildingItems.length; i++){
            featuresBuildingItems[0].click();
            featuresBuildingRow[i].onclick = () => {
                featuresBuildingItems.forEach((el) => {el.classList.toggle('features__building_items_active');})
                featuresBuildingRow[i].classList.toggle('features__building_row--active');
            }
        }

        for(let i = 0; i < featuresBuildingItem.length; i++){
            featuresBuildingItem[i].onclick = (event) => {
                featuresScreenMobile.forEach((el) => {el.classList.remove('features__building_img--active')});
                featuresBuildingRow.forEach((el) => {el.classList.remove('features__building_row--active')});
                featuresBuildingRow[0].innerHTML = event.target.innerHTML;
                featuresBuildingItems.forEach((el) => {el.classList.toggle('features__building_items_active');});
                featuresScreenMobile[i].classList.add('features__building_img--active');
            }
        }
})

function positionBlock() {
    let width = window.innerWidth;
    switch (true) {
        case width < 960 && width > 639:
            if (leftInfo || rightInfo) {
                leftInfo.style.left = infoContent.getBoundingClientRect().left + 'px';
                rightInfo.style.right = infoContent.getBoundingClientRect().x + 28 + 'px';
            }
            if (helpBg) {
                helpBg.classList.remove('help__mobile');
            }

            if (started) {
                started.classList.add('started__mobile');
            }
            break;

        case width < 639 && width > 479:
            if (leftInfo || rightInfo) {
                leftInfo.style.left = infoContent.getBoundingClientRect().left - 10 + 'px';
                rightInfo.style.right = infoContent.getBoundingClientRect().x + 10 + 'px';
            }
            if (helpBg) {
                helpBg.classList.add('help__mobile');
            }
            if (started) {
                started.classList.add('started__mobile');
            }
            break;

        case width < 479:
            if (helpBg) {
                helpBg.classList.add('help__mobile');
            }
            if (started) {
                started.classList.add('started__mobile');
            }
            break;

        default:
            if (leftInfo || rightInfo) {
                leftInfo.style.left = 20 + 'px';
                rightInfo.style.right = 20 + 'px';
            }
    }
}

for (let i = 0; i < tabsBlockTitle.length; i++) {
    tabsBlockTitle[i].addEventListener('click', () => {
        tabsBlockContent[i].classList.toggle('tabs__block_content--active');
        tabsBlockTitle[i].classList.toggle('tabs__block_title--open');
    });
}

positionBlock();

window.addEventListener('resize', (event) => {
    positionBlock();
});

// ====== YOUTUBE VIDEO =====
function findVideos() {
    let videos = document.querySelectorAll('.video');

    for (let i = 0; i < videos.length; i++) {
        setupVideo(videos[i]);
    }
}
function setupVideo(video) {
    let link = video.querySelector('.video__link');
    let media = video.querySelector('.video__media');
    let button = video.querySelector('.video__button');
    let id = parseMediaURL(media);
    video.addEventListener('click', () => {
        let iframe = createIframe(id);

        link.remove();
        button.remove();
        video.appendChild(iframe);
    });

    link.removeAttribute('href');
    video.classList.add('video--enabled');
}
function parseMediaURL(media) {
    let regexp = /https:\/\/i\.ytimg\.com\/vi\/([a-zA-Z0-9_-]+)\/maxresdefault\.jpg/i;
    let url = media.src;
    let match = url.match(regexp);

    return match[1];
}
function createIframe(id) {
    let iframe = document.createElement('iframe');
    iframe.setAttribute('allowfullscreen', '');
    iframe.setAttribute('allow', 'autoplay');
    iframe.setAttribute('src', generateURL(id));
    iframe.classList.add('video__media');
    return iframe;
}
function generateURL(id) {
    let query = '?rel=0&showinfo=0&autoplay=1';

    return 'https://www.youtube.com/embed/' + id + query;
}
findVideos();

// ====== WEBP BG =====
function testWebP(callback) {
    var webP = new Image();
    webP.onload = webP.onerror = function () {
        callback(webP.height == 2);
    };
    webP.src =
        'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';
}

testWebP(function (support) {
    if (support == true) {
        document.querySelector('body').classList.add('webp');
    } else {
        document.querySelector('body').classList.add('no-webp');
    }
});
